var express = require('express');
var path = require('path');
var config = require('./config')

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));

const PORT = config.PORT;

var router = express.Router();

router.get('/', function (request, response) {
  response.render('index', { title: 'Homepage!' });
});

router.get('/student', function (request, response) {
  response.render('index', { title: 'Welcome, student! Update' });
});

router.get('/teacher', function (request, response) {
  response.render('index', { title: 'Welcome, teacher! Update' });
});

app.use('/', router);

app.listen(PORT, function () {
  console.log('Listening on port ' + PORT);
});
